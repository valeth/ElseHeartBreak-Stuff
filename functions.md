# Heart
EnergyLevel
GetNumericData
SetNumericData
SetPosition
GetPosition
ZapPersonGently
Break

# MainFrame
GetAction(name)
GetPeople()
GetThingsOfType(type)
GetPosition(name)
GetRoom(name)
GetThingsInRoom(room)
GetAllRooms()
GetTypeOfThing(name)
InteractWith(name, target)
SetPosition(name, targetThing)

# Hugin
Lock(doorName)
Unlock(doorName)
FindPath(start, goal)

# PowerTap
TurnOn(lampName)
TurnOff(lampname)

# MeteorologyServer
SetRain(amount)
GetRain()
